# structure

## WURCS

### SPARQL Endpoint

* https://sparql.glyconavi.org/sparql

### SPARQL

```
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix glyconavi: <http://glyconavi.org/ontology/pdb#>
prefix glycan: <http://purl.jp/bio/12/glyco/glycan#>
prefix dcterms: <http://purl.org/dc/terms/>
prefix pdbo:  <https://rdf.wwpdb.org/schema/pdbx-v50.owl#>
prefix glyconavi:  <http://glyconavi.org/ontology/pdb#>
#SELECT DISTINCT ?pdb_entry ?entry_id ?date ?wurcs ?id 
SELECT DISTINCT ?entry_id ?wurcs
FROM <http://glyconavi.org/tcarp>
WHERE {
?pdb_entry glyconavi:has_branch_entry ?branch_entry ;
 rdf:type glyconavi:TCarpEntry ;
 dcterms:identifier ?entry_id ;
 dcterms:created ?date .
?branch_entry rdf:type glyconavi:BranchEntry .
?branch_entry glycan:has_glycan ?glycan .
?glycan glycan:has_glycosequence ?glycosequence .
?glycosequence glycan:has_sequence ?wurcs .
?glycosequence glycan:in_carbohydrate_format glycan:carbohydrate_format_wurcs .
OPTIONAL { ?glycan glyconavi:has_glytoucan_id ?id . }
#OPTIONAL { ?glycan glyconavi:has_aglycone ?aglycone . }
}
order by ?entry_id ?wurcs
```

### Result

* pdbid-glycan.tsv (2023-05-23)




## branch entity model

```
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix glyconavi: <http://glyconavi.org/ontology/pdb#>
prefix glycan: <http://purl.jp/bio/12/glyco/glycan#>
prefix dcterms: <http://purl.org/dc/terms/>
prefix pdbo:  <https://rdf.wwpdb.org/schema/pdbx-v50.owl#>
prefix glyconavi:  <http://glyconavi.org/ontology/pdb#>
#SELECT DISTINCT ?pdb_entry ?entry_id ?date ?wurcs ?id 
SELECT DISTINCT ?entry_id ?model_id ?wurcs
FROM <http://glyconavi.org/tcarp>
WHERE {
?pdb_entry glyconavi:has_branch_entry ?branch_entry ;
 rdf:type glyconavi:TCarpEntry ;
 dcterms:identifier ?entry_id ;
 dcterms:created ?date .
?branch_entry rdf:type glyconavi:BranchEntry .
?branch_entry glycan:has_glycan ?glycan .
optional { ?branch_entry glyconavi:has_model_id ?model_id .
  FILTER ( ?model_id != "2" )
 }
?glycan glycan:has_glycosequence ?glycosequence .
?glycosequence glycan:has_sequence ?wurcs .
?glycosequence glycan:in_carbohydrate_format glycan:carbohydrate_format_wurcs .
OPTIONAL { ?glycan glyconavi:has_glytoucan_id ?id . }
#OPTIONAL { ?glycan glyconavi:has_aglycone ?aglycone . }
}
order by ?entry_id ?wurcs
```

### Result




## SPARQL

* https://sparqlist.glyconavi.org/TCarp_glycosylated_site?id=1A39&graph=http%3A%2F%2Fglyconavi.org%2Ftcarp

```
DEFINE sql:select-option "order"
prefix owl:     <http://www.w3.org/2002/07/owl#>
prefix xsd:     <http://www.w3.org/2001/XMLSchema#>
prefix skos:    <http://www.w3.org/2004/02/skos/core#>
prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#>
prefix wwpdb:   <https://rdf.wwpdb.org/pdb/>
prefix glycan:  <http://purl.jp/bio/12/glyco/glycan#>
prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix pdbo:    <https://rdf.wwpdb.org/schema/pdbx-v50.owl#>
prefix pdbcc:   <https://rdf.wwpdb.org/cc/>
prefix sio:     <http://semanticscience.org/resource/>
prefix dcterms:  <http://purl.org/dc/terms/>
prefix uniprot:  <http://purl.uniprot.org/uniprot/>
prefix dc:      <http://purl.org/dc/elements/1.1/>
prefix glyconavi:  <http://glyconavi.org/ontology/pdb#>
SELECT DISTINCT ?id ?strand_id as ?mod_chain ?model_num ?label_alt_id ?protein_db ?db_name
  ?mod_comp_id as ?mod_aa ?sequon ?neighboring_sequence
 ?glycan_type_label as ?gtype ?mod_pos ?gtc ?wurcs #?ctfile
#SELECT DISTINCT ?model_type
FROM <http://glyconavi.org/tcarp>
WHERE {
  ?TcarpEntry a glyconavi:TCarpEntry ;
              dcterms:identifier ?id .
  FILTER ( ?id = "1AA5" ) 
  # Strand
  ?TcarpEntry glyconavi:has_strand ?strand .
  ?strand dcterms:identifier ?protein_db ;
          a ?strand_type ;
          glyconavi:has_strand_id ?strand_id ;
          glyconavi:has_db_name ?db_name ;
          glyconavi:has_entity_id ?entity_id ;
          glyconavi:has_sequence ?aa_seq .
  # PDBModel
  ?TcarpEntry glyconavi:has_PDB_model_glycan ?model_glycan .
  ?model_glycan  glyconavi:has_model ?model .
 OPTIONAL { ?model glyconavi:has_label_alt_id ?label_alt_id . }
  ?model a glyconavi:PDBModel ;
        #glyconavi:has_ctfile ?ctfile ;
        glyconavi:has_pdbx_PDB_model_num ?model_num .
  # Interaction (optional)
 # OPTIONAL { ?model glyconavi:has_interaction ?interaction . }
  # Glycan and Glycosequence
  ?model glycan:has_glycan ?glycan .
  ?glycan ?a glyconavi:WURCSStandard ;
           glycan:has_glycosequence ?glycosequence .
  ?glycosequence a glycan:Glycosequence ;
                 glycan:has_sequence ?wurcs ;
                 glycan:in_carbohydrate_format glycan:carbohydrate_format_wurcs .
  OPTIONAL { ?glycan glyconavi:has_glytoucan_id ?gtc . }
  # GlycosylationSite
  ?model_glycan glyconavi:has_glycosylation_site ?ptm_site .
  ?ptm_site a glyconavi:GlycosylationSite ;
            glyconavi:is_on_strand ?strand ;
            glyconavi:has_one_letter_code ?one_letter_code ;
            glyconavi:has_sequence_before ?sequence_before ;
            glyconavi:has_sequence_after ?sequence_after ;
            glyconavi:has_asym_id ?mod_asym_id ;
            glyconavi:has_entity_id ?mod_entity_id ;
            glyconavi:has_seq_id ?mod_pos ;
            glyconavi:has_asym_id ?mod_asym_id ;
            glyconavi:has_comp_id ?mod_comp_id ;
            glyconavi:has_atom_id ?mod_atom_id ;
            glyconavi:has_glycan_type ?glycan_type .
  # URI for pdbx_struct_mod_residue (optional)
  OPTIONAL { ?ptm_site glyconavi:is_mod_residue ?mod_residue . }
   FILTER (STRLEN(?sequence_after) = 3)
  BIND (CONCAT(?one_letter_code, substr(?sequence_after, 1, 2)) as ?sequon)
  BIND (CONCAT(?sequence_before,?sequon) as ?neighboring_sequence)  
  OPTIONAL { ?ptm_site glyconavi:has_helix_info ?helix_info . }
  OPTIONAL { ?ptm_site glyconavi:has_sheet_info ?sheet_info . }
  ?glycan_type a glyconavi:GlycanType;
               rdfs:label ?glycan_type_label .
}
ORDER BY ?id ?mod_chain ?model_num ?label_alt_id  ?protein_db ?db_name ?mod_pos ?wurcs
```



### Organism

```
prefix pdbo:  <https://rdf.wwpdb.org/schema/pdbx-v50.owl#>
SELECT   DISTINCT ?organism (COUNT (?organism) AS ?count )
FROM <http://glyconavi.org/tcarp>
WHERE {
?s pdbo:entity_src_nat.pdbx_organism_scientific ?organism  .
FILTER ( ?organism != "" )
}
ORDER BY DESC (?count)
```


### Glycoprotein

```
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix dcterms:  <http://purl.org/dc/terms/>
prefix up: <http://purl.uniprot.org/core/>
prefix pdbo:  <https://rdf.wwpdb.org/schema/pdbx-v50.owl#>
prefix glyconavi:  <http://glyconavi.org/ontology/pdb#>
SELECT DISTINCT ?id ?protein_db  ?title ?descriptor
FROM <http://glyconavi.org/tcarp>
FROM <http://glyconavi.org/Uniprot-Glycoprotein-Reviewed-2020-06-05>
WHERE {
?TcarpEntry   glyconavi:has_protein_db ?pro_db ;
       a glyconavi:TCarpEntry ;
       dcterms:identifier ?id ;
       glyconavi:has_struct ?struct .
?struct a pdbo:struct ;
        pdbo:struct.pdbx_descriptor  ?descriptor  ;
        pdbo:struct.title ?title .
?pro_db dcterms:identifier ?protein_db .
BIND (URI(CONCAT("http://purl.uniprot.org/uniprot/", ?protein_db)) as ?up)
?up rdf:type up:Protein ;
  up:reviewed true .
}
```
