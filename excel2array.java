import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ExcelReader {
    public static void main(String[] args) {
        String filePath = "path/to/your/excel/file.xlsx";
        String sheetName = "Sheet1";
        int startRow = 1; // データの読み取りを開始する行（1行目はヘッダーなどのためスキップ）
        int startColumn = 0; // データの読み取りを開始する列

        File file = new File(filePath);
        if (!file.exists()) {
            System.out.println("指定したファイルが存在しません。");
            return;
        }

        try {
            FileInputStream fis = new FileInputStream(file);
            Workbook workbook = WorkbookFactory.create(fis);
            Sheet sheet = workbook.getSheet(sheetName);

            int rowCount = sheet.getLastRowNum() + 1;
            int columnCount = sheet.getRow(0).getLastCellNum();
            Object[][] data = new Object[rowCount - startRow][columnCount - startColumn];

            for (int i = startRow; i < rowCount; i++) {
                Row row = sheet.getRow(i);
                for (int j = startColumn; j < columnCount; j++) {
                    Cell cell = row.getCell(j);
                    data[i - startRow][j - startColumn] = getCellValue(cell);
                }
            }

            // 配列データの表示（例）
            for (Object[] row : data) {
                for (Object cellData : row) {
                    System.out.print(cellData + "\t");
                }
                System.out.println();
            }

            workbook.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Object getCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue();
                } else {
                    return cell.getNumericCellValue();
                }
            case BOOLEAN:
                return cell.getBooleanCellValue();
            case FORMULA:
                return cell.getCellFormula();
            default:
                return null;
        }
    }
}
